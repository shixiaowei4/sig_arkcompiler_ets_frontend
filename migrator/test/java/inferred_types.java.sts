/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ohos.migrator.tests.java;

import * from "std/math/math";
import List from "java/util";
open class Main  {
// Java language allows to omit return/parameter types for lambda expression.
// These types are then inferred by the compiler.
// This test covers all kinds of types that can be inferred by compiler
    inner open class C  {
    }

    class E extends Enum<E>  {
        public static values(): E[] {
            return [];
        }
        public static valueOf(name : String): E {
            for (let value : E of values()){
                if (name == value.toString()) return value;
            }
            return null;
        }
        private constructor(name : String, ordinal : int) {
            super(name, ordinal);
        }

    }

    interface IPrimitiveTypes {
        M(i : int, b : boolean, c : char): void ;
    } // Void, primitive types

    interface IClassTypes {
        M(c : C, s : String, e : E): IPrimitiveTypes ;
    } // Class, Interface, Enum

    interface IArrays {
        M(sArray : String[], iArray : IPrimitiveTypes[][], eArray : E[][]): int[] ;
    } // Arrays

    interface IParametrizedTypes {
        M(list : List<Int>, listOfArrays : List<IPrimitiveTypes[]>, listOfLists : List<List<String>>[]): List<> ;
    } // Parametrized type, Raw type

    interface IWildcards {
        M(listExtends : List<out Integral>, listSuper : List<in Int>): List<out> ;
    } // Wildcards

    interface IGeneric<T> {
        M(list : List<T>): T ;
    } // Generic interface (parametrized types, type variables, wildcards, etc.)

    open class OuterClass<T>  {
        inner open class InnerClass  {
            open M(t : T): void {
                let l : IGeneric<T> = (list : java.util.List<T>): T => list.get(0); // Generic functional interface parametrized with type variable
            }
        }

    }

    interface IInnerOuter {
        M(inner : OuterClass<List<String>>.InnerClass): int ; // Nested type (with parametrized type)
    }

    open Test(): void {
        let l1 : IPrimitiveTypes = (i : int, b : boolean, c : char): void => {
        }
; // Void, primitive types
        let l2 : IClassTypes = (c : com.ohos.migrator.tests.java.Main.C, s : String, e : com.ohos.migrator.tests.java.Main.E): com.ohos.migrator.tests.java.Main.IPrimitiveTypes => l1; // Class, Interface, Enum
        let l3 : IArrays = (sArray : String[], iArray : com.ohos.migrator.tests.java.Main.IPrimitiveTypes[][], eArray : com.ohos.migrator.tests.java.Main.E[][]): int[] => new int[5]; // Arrays
        let l4 : IParametrizedTypes = (list : java.util.List<Int>, listOfArrays : java.util.List<com.ohos.migrator.tests.java.Main.IPrimitiveTypes[]>, ListOfLists : java.util.List<java.util.List<String>>[]): java.util.List<> => list; // Parametrized type, Raw type
        let l5 : IWildcards = (listExtends : java.util.List<out Integral>, listSuper : java.util.List<in Int>): java.util.List<out> => listExtends; // Wildcards
// Generic functional interface
        let l6 : IGeneric<Integral> = (list : java.util.List<Integral>): Integral => list.get(0); // parametrized with explicit type
        let l7 : IGeneric<out> = (list : java.util.List<Object>): Object => list.get(0); // parametrized with wildcard
        let l8 : IGeneric<out Integral> = (list : java.util.List<Integral>): Integral => list.get(0); // parametrized with wildcard with bound
        let l9 : IGeneric<in Integral> = (list : java.util.List<Integral>): Integral => list.get(0); // parametrized with wildcard with bound
        let l10 : IInnerOuter = (inner : com.ohos.migrator.tests.java.Main.OuterClass<java.util.List<java.lang.String>>.InnerClass): int => 0; // Nested type (with parametrized type)
    }
}

